<?php

/**
 * @file
 * Provides a FieldAPI field type to display a BrightTalk player.
 */

/**
 * Implements hook_theme().
 */
function brighttalk_field_theme($existing, $type, $theme, $path) {
  return [
    'brighttalk_field_player' => [
      'variables' => ['channel_id' => 0, 'webcast_id' => NULL],
    ],
  ];
}

/**
 * Grab parameter values from embed code so it can be validated and sanitised.
 *
 * @param string $embed_code
 *   A block of embed code.
 * @param string $parameter
 *   'channelid' for the Channel ID or 'communicationid' for the Webcast ID.
 *
 * @return {number}
 *   Either the Channel ID or Webcast ID.
 */
function brighttalk_field_webcast_code_value($embed_code, $parameter) {
  if (empty($embed_code)) {
    return NULL;
  }

  $delimiter = '<param name="' . $parameter . '" value="';
  $parts_start = explode($delimiter, $embed_code);

  if (!isset($parts_start[1])) {
    return NULL;
  }

  $parts = explode('"', $parts_start[1]);

  if (!isset($parts[0]) || empty($parts[0]) || !is_numeric($parts[0])) {
    return NULL;
  }

  $value = $parts[0];
  return $value;
}
